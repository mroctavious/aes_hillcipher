
void EncriptarCPU(uchar4* const imagenOriginal,
	uchar4* const imagenSalida,
	int filas, int columnas, int sizePack)
{

	int paquetes = ((((filas*columnas) * 3) / sizePack) + 1);
	//printf("Filas %d  Columnas %d  Paquetes %d \n", filas, columnas, paquetes);

	int valorC1 = 0;
	int valorC2 = 0;
	int valorC3 = 0;
	unsigned char newValorC1;
	unsigned char newValorC2;
	unsigned char newValorC3;
	unsigned char mClaveExp[176]; // contendra la expansion de clave	
	unsigned char clave[16] = { 0x6c, 0x61, 0x20, 0x76, 0x69, 0x64, 0x61, 0x20, 0x64, 0x73, 0x20, 0x62, 0x65, 0x6c, 0x6c, 0x61 }; //clave    
	unsigned char arrImagen[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	unsigned char texto[4][4]; // matriz 4x4 texto a cifrar
	unsigned char mClave[4][4]; //matriz 4x4 clave


	for (int i = 0; i < (filas*columnas); i++)
	{

		valorC1 = imagenOriginal[i].x;
		valorC2 = imagenOriginal[i].y;
		valorC3 = imagenOriginal[i].z;

		newValorC1 = hexadecimal(valorC1);
		newValorC2 = hexadecimal(valorC2);
		newValorC3 = hexadecimal(valorC3);

		imagenSalida[i].x = (uchar)newValorC1;
		imagenSalida[i].y = (uchar)newValorC2;
		imagenSalida[i].z = (uchar)newValorC3;
		imagenSalida[i].w = imagenOriginal[i].w;  // alfa

	}
	for (int i = 0; i < 4; i++){
		for (int j = 0; j < 4; j++){
			mClave[j][i] = clave[i * 4 + j]; // convierte clave a cifrar en una matriz 4X4
		}
	}

	int pixel = 0;
	int canal = 0;
	unsigned char valor = 0x00;
	int posini = 0;
	int posfin = 0;
	unsigned char newvalor1 = 0, newvalor2 = 0, newvalor3 = 0;

	for (int c = 0; c < paquetes; c++){
		posini = c * sizePack;
		posfin = posini + (sizePack - 1);
		int a = 0;

		int posMat = 0;
		for (a = posini; a <= posfin; a++){
			canal = a % 3;
			pixel = a / 3;
			valor = 0x00;
			if (pixel<(filas*columnas)){
				if (canal == 0){
					valor = imagenSalida[pixel].x;
				}
				else{
					if (canal == 1){
						valor = imagenSalida[pixel].y;
					}
					else
					{
						valor = imagenSalida[pixel].z;
					}
				}
				arrImagen[posMat] = valor;
			}
			posMat++;
		}

		for (int i = 0; i < 4; i++){
			for (int j = 0; j < 4; j++){
				texto[j][i] = arrImagen[i * 4 + j]; // texto a cifrar en una matriz 4X4
			}
		}

		expandirClave(mClave, mClaveExp);
		encriptar(texto, mClaveExp);

		a = posini;
		for (int i = 0; i < 4; i++){
			for (int j = 0; j < 4; j++){
				pixel = a / 3;
				canal = a % 3;
				valor = texto[j][i];
				if (pixel < (filas*columnas)){
					if (canal == 0){
						newvalor1 = decimal(valor);
						imagenSalida[pixel].x = newvalor1;
					}
					else{
						if (canal == 1){
							newvalor2 = decimal(valor);
							imagenSalida[pixel].y = newvalor2;
						}
						else{
							newvalor3 = decimal(valor);
							imagenSalida[pixel].z = newvalor3;
						}
					}
					imagenSalida[pixel].w = imagenOriginal[pixel].w;
				}
				a=a+1;
			}
		}
	}
}


float CompararImgs(uchar4 *imagen1, uchar4*imagen2, int rows, int cols, int *elemDifer) {
	int diferentes = 0;
	int sumaR, sumaG, sumaB;
	sumaR = 0; sumaG = 0; sumaB = 0;
	float difTotal = 0;
	for (int i = 0; i< (rows*cols); i++) {
		sumaR = imagen1[i].x - imagen2[i].x;
		sumaG = imagen1[i].y - imagen2[i].y;
		sumaB = imagen1[i].z - imagen2[i].z;
		if ((sumaR + sumaG + sumaB)>0) {
			diferentes = diferentes + 1;
			difTotal = difTotal + sumaR + sumaG + sumaB;
		}

	}
	*elemDifer = diferentes;
	return difTotal;

}

void PrintTxt(uchar4 *imagen, int rows, int cols)
{
	FILE *f = fopen("salida1.txt", "w");
	if (f == NULL)
	{
		cout << "Error al abrir fichero.txt";
	}

	int valmax = 26;
	for (int i = 0; i < (cols*rows); i++)
	{
		int val = (imagen[i].x + imagen[i].y + imagen[i].z) / (valmax * 3);
		if (f == NULL) {
			printf("%d ", val);
		}
		else {
			fprintf(f, "%d ", val);
		}
		if (i % cols == 0 && i != 0) {
			if (f == NULL) {
				printf("\n");
			}
			else {
				fprintf(f, "\n");
			}
		}
	}
	fclose(f);
}
