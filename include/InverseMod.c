//	######################################################
//	#####		MODULAR INVERSE MATRIX		######
//	#####		RETURNS MATRIX	WITH		######
//	#####		THE MATRIX THE PIXELS		######
//	#####		WILL BE DECRYPTED		######
//	######################################################


//Function which will print the array.... Debugging purpuses
void printArray(int *array, int n)
{
	int p=0;
	while(p<n)
	{
		printf("Pos:%d    Content:%d\n",p, array[p]);
		p++;
	}

}


//Get determinant of a 4 X 4 matrix
int determinant4 ( int *matrix, int n)
{
	int tempMatrix[n*n];
	int s=1;
	int determinante=0;

	if ( n == 1 )
	{
		return matrix[0];
	}
	else
	{
		int q, i, j;
		for( q=0; q < n; q++)
		{
			int w=0, z=0;
			for ( i=0; i < n; i++)
			{
				for( j=0; j < n; j++)
				{
					tempMatrix[i*n+j]=0;
					if ( i != 0 && j != q)
					{
						tempMatrix[(w*n)+z]=matrix[(i*n)+j];
						if (z<(n-2))
						{
							z++;
						}
						else
						{
							z=0;
							w++;
						}
					}
				}
			}
			determinante+= (s * ( matrix[q] * determinant4(tempMatrix,n-1)));
			s = -1 * s;
		}
	}
	return (determinante);
}



//Lineal formula for calculating the determinant of a 3 X 3 matrix
int determinant3(int *matrix)
{
	int a=matrix[0];
	int b=matrix[1];
	int c=matrix[2];
	int d=matrix[3];
	int e=matrix[4];
	int f=matrix[5];
	int g=matrix[6];
	int h=matrix[7];
	int i=matrix[8];

	//Math formula which will get the determinant of a 3X3 matrix
	return ( ( ( a * e * i ) + ( d * h * c ) + ( g * b * f ) ) - ( ( c * e * g ) + ( f * h * a ) + ( b * d * i)) );

}




int determinant2(int *matrix)
{
	int a=matrix[0];
	int b=matrix[1];
	int c=matrix[2];
	int d=matrix[3];

	return ( ( a * b ) - ( c * b ) );

}


//Greatest Common Divisor sequential
int gcd ( int a, int b )
{
	int c;
	while ( a != 0 )
	{
		c = a;
		a = b % a;
		b = c;
	}
	return b;
}

// Recursive Greatest Common Divisor
int gcdr ( int a, int b )
{
	if ( a==0 )
	{
		return b;
	}
	return gcdr ( b % a, a );
}


//Function which will get the absolute of a number
int abs(int number)
{
	int return_var=0;
	if (number < 0)
	{
		return_var=number * -1;
	}
	else
	{
		return_var=number;
	}
	return return_var;
}

//Get the coFactor with formula
int coFactorFormula(int *matrix, int n)
{
	//The integer number which will be returned
	int result=0;

	//Switch for the formula depending of the position
	if(n==0)
	{
		result=( ( matrix[4] * matrix[8] ) - ( matrix[5] * matrix[7] ) );
	}
	else if(n==1)
	{
		result=-( ( matrix[3] * matrix[8] ) - ( matrix[5] * matrix[6] ) );

	}
	else if(n==2)
	{
		result=( ( matrix[3] * matrix[7] ) - ( matrix[4] * matrix[6] ) );

	}
	else if(n==3)
	{
		result=-( ( matrix[1] * matrix[8] ) - ( matrix[2] * matrix[7] ) );

	}
	else if(n==4)
	{
		result=( ( matrix[0] * matrix[8] ) - ( matrix[2] * matrix[6] ) );

	}
	else if(n==5)
	{
		result=-( ( matrix[0] * matrix[7] ) - ( matrix[1] * matrix[6] ) );

	}
	else if(n==6)
	{
		result=( ( matrix[1] * matrix[5] ) - ( matrix[2] * matrix[4] ) );

	}
	else if(n==7)
	{
		result=-( ( matrix[0] * matrix[5] ) - ( matrix[2] * matrix[3] ) );

	}
	else if(n==8)
	{
		result=( ( matrix[0] * matrix[4] ) - ( matrix[1] * matrix[3] ) );

	}

	return result;
}

//Modular Multiplicative Inverse
int modular_inverse_multiplicative(int determinant)
{
	///////////////////////////////////////
	//FORMULA//////////////////////////////
	//	X=( ( MODULE * i) + 1 ) / R  //
	///////////////////////////////////////
	//printf("modular_inverse_multiplicative now1\nDeterminant:%d\n",determinant);

	//Iterator variable
	int i;
	//printf("modular_inverse_multiplicative now2\n");
	//Get the euclidan module of the determinant and save it in r, this will be used to get the inverse(part of the formula).
	int r=mymod(determinant, MODULE);
	//printf("modular_inverse_multiplicative now3\n");

	//Applying brute force to get the number which will feet to the first INTEGER number
	for(i=1; i<9457; i++)
	{
		//Apply the formula
		double x=( ( (double) MODULE * (double) i ) + 1 ) / (double) r;

		//printf("x=( ( %d * %d ) + 1 ) / %d\n",MODULE,i,r);
		//printf("x=%f\n",x);

		//Check if its an integer number
		if(isInt(x) == true)
		{
			return (int) x;
		}
	}
	return 0;


}

//Generate a random matrix compatible with the algorithm
void generate_matrix(int n, int randSeed, int *generated_matrix)
{
	int i;
	int i_continue=1;
	//Create new seed from time
	if(randSeed % 2 == 0)
	{
		randSeed=-randSeed;
	}
	srand((int)time(NULL)+randSeed);

	while(i_continue==1)
	{

		//If only RGB
		if( n == 3 )
		{
			//Generate a random number for each position of the key
			for( i=0; i<n*n; i++ )
			{
				generated_matrix[i]=mymod((int)rand(),256);
			}

			//Get the determinant of the matrix
			int determinante=determinant3(generated_matrix);

			//Check if its a valid matrix and if its possible the solution
			if ( gcdr(abs(determinante),MODULE) == 1 )
			{
				//Key is compatible
				i_continue=0;
			}
			else
			{
				//In case of failure
				//Clean everything and call the function again
				for( i=0; i<n*n; i++ )
				{
					generated_matrix[i]=0;
				}
			}
		}
	}
}



//Transpose the matrix
void transpose_matrix(int *cofactor, int *transpose_matrix, int n)
{
	int i, j;
	for( i=0; i < n; i++)
	{
		for(j=0; j < n; j++)
		{
			transpose_matrix[(i*n)+j]=cofactor[(j*n)+i];
		}
	}

}

//Euclidan module
int mymod (int n, int m)
{
	if( n < 0 )
	{
		if ( ((n % m) + m) == m )
		{
			return 0;
		}
		else
		{
			return ((n % m) + m);
		}
	}
	else
	{
		return n % m;
	}
}


//This function will calculate if its an integer number
bool isInt(double number)
{
	int integer=(int) number;
	return (number == integer);
}


int arrayLength(int *array)
{
	int how_many=0;
	int *arrayx=array;
	while ( *array != '\0')
	{
		printf("%d\n",array[how_many]);
		how_many++;
	}
	return array - arrayx;
}
void InverseMatrix (int *originalKey, int channels, int *inverse_array)
{
	int inverse_matrix[channels*channels];

	//CAPTURE THE KEY FROM THE ARGUMENTS
	int *key=originalKey;
	int i;

	//If the matrix has Alpha channel(4X4)
	if ( channels == 4 )
	{
		//int d=determinant4(key,4);
		printf("Determinante: 4X4=%d\n",0);
	}
	//Otherwise it does not have alpha channel(3X3)
	else
	{
		//Get the determinant of the matrix
		int determinante=determinant3(key);

		//Check if its a valid matrix and if its possible the solution
		if ( gcdr(abs(determinante),MODULE) != 1 )
		{
			//Print error and end program
			sorry;
		}
		else
		{
			//Create the arrays for the matrix for each step of the algorithm
			int coFactor_array[9];
			int transpose_array[9];
			int i;
			//ConseguirCoFactores
			for(i=0; i<9;i++)
			{
				//Se llama funcion que contiene la formula matematica
				coFactor_array[i]=(coFactorFormula(key,i));
			}

			//Now transpose the matrix
			transpose_matrix(coFactor_array, transpose_array, 3);

			int mim=modular_inverse_multiplicative(determinante);
			for(i=0;i<9;i++)
			{
				inverse_matrix[i]=mymod((mim * transpose_array[i]), MODULE );
				inverse_array[i]=inverse_matrix[i];
				//printf("inverse: %d\n",inverse_matrix[i]);
			}

		}

	}

}
