//
#include "opencv2/opencv.hpp"

using namespace cv;
// Abre el video
int main(int, char**)
{
    CvCapture cap(0); // open the default camera
    if(!cap.isOpened())  // check if we succeeded
        return -1;

    Mat edges;
    namedWindow("edges",1);
    for(;;)
    {
        Mat frame;
        cap >> frame; // get a new frame from camera
        cvtColor(frame, edges, COLOR_BGR2GRAY);
        GaussianBlur(edges, edges, Size(7,7), 1.5, 1.5);
        Canny(edges, edges, 0, 30, 3);
        imshow("edges", edges);
        if(waitKey(30) >= 0) break;
    }
    // the camera will be deinitialized automatically in VideoCapture destructor
    return 0;
}


//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//		  FUNCTIONS HERE :D
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
int tochCryptVideo(char *inputVideo, char *output, char fCodec[])
{

	////////////////////////////////////////////
	//					////
	//	Start to open video		////
	////////////////////////////////////////////
	char *outputVid=(char *) malloc( sizeof(char) * (strlen(output) + 7) );
	memset(outputVid, '\0', sizeof(outputVid) );
	strcpy(outputVid, output);
	strcat(outputVid, "C.avi");
	char *outputVideoFile=(char *) malloc( sizeof(char) * (strlen(output) + 40) );
	memset(outputVideoFile, '\0', sizeof(outputVideoFile) );
	strcpy(outputVideoFile, output);
	strcat(outputVideoFile, "_0000000.png");
	printf("PKO Salida es %s\n",outputVideoFile);
//Intentar abrir archivo:
	//Abrir video y guardar en vCap
	VideoCapture vCap(inputVideo);

	//How many channel we will modify, 3 or 4
	int channels=3;
	int i,j;

	//Verificar si NO se pudo abrir el archivo
	if( ! vCap.isOpened() )
	{
		tochHelp(5);
		return 5;
	}

//Conseguir datos del video de entrada:

	//Conseguimos los frames por segundo FPS
	double fps= vCap.get(CV_CAP_PROP_FPS);
	//Resolucion eje X
	int ancho=vCap.get(CV_CAP_PROP_FRAME_WIDTH);
	//Resolucion eje Y
	int largo=vCap.get(CV_CAP_PROP_FRAME_HEIGHT);
	//Numero total de frames en el video
	float totalFrames=(float) vCap.get(CV_CAP_PROP_FRAME_COUNT);

	printf("FPS:%f\nAncho:%d   Largo:%d\nTotalFrames:%f\n",fps,ancho,largo,totalFrames);
	float frameNum=0.0;

	//Crear objetos que guardaran y reproduciran el video:

	//Crear estructura donde guardaremos el video
	VideoWriter outputVideo;
	VideoWriter outputVideoTmp;

	//Crear constructor
	//int fourccNum=CV_FOURCC('R','G','B','T');
	int fourccNumPng=CV_FOURCC('R','G','B','T');
	int fourccNumVid=CV_FOURCC('M', 'J', 'P', 'G');
	outputVideo.open(outputVideoFile, fourccNumPng, fps, Size(ancho, largo));
	outputVideoTmp.open(outputVid, fourccNumVid, fps, Size(ancho, largo));
	//Verifica que se haya abierto el archivo
	if( ! outputVideo.isOpened() || ! outputVideoTmp.isOpened())
	{

		tochHelp(6);
		return 6;
	}

	int contadorImagenes=0;
	float totalTime=0;

	//Recorremos frame por frame:
	//Ciclo infinito para reproducir el video
	while(0==0)
	{
		//Creamos structura tipo Mat
		Mat foto;

		//Llamamos la funcion read() y lo guardaremos en la estructura foto
		bool hayFotograma = vCap.read(foto);

		//Verificamos si hay fotograma, de lo contrario significa que acabo
		if (! hayFotograma)
		{
			printf("\nNo hay mas fotogramas\n");
			break;

		}
		float *timing=(float *) malloc(sizeof(float) * 1);
		Mat encrypt=AEScrypt(foto, timing);
		totalTime=totalTime+*timing;

		//Mat Decrypt=Tochcrypt(encrypt,inverseMatrix);
		//Mostrar fotograma en la 'ventana del video'
		//imshow("Video Original", foto);
		//imshow("TochCrypt :)", encrypt);
		//imshow("TochCrypt Decrypt :)", Decrypt);

		//Escribir el fotograma en el archivo
		outputVideo.write(encrypt);
		outputVideoTmp.write(encrypt);

		//Liberamos las estructuras de opencv
		foto.release();
		encrypt.release();
		//Decrypt.release();
		free(timing);
		//Agregamos 1 para obtener el porcentaje
		frameNum++;
		printf("\rCurrent Progress: %.2f %%", ((frameNum * 100.0) / totalFrames));

		//Es necesario esta parte, de lo contrario no muestra nada
		if(cvWaitKey(33) == 27)
		{
			break;
		}

	}
	printf("Suma de tiempos:%.4f\nTotal Frames:%f\nPromedio:%.4f\n", totalTime, totalFrames, totalTime/totalFrames);
	outputVideo.release();
	outputVideoTmp.release();
	//free(inverseMatrix);
	free(outputVideoFile);
	free(outputVid);
	destroyAllWindows();

	return 0;
}




void tochHelp(int error)
{
	if(error == 0)
		printf( "Tochcrypt will encrypt your videos so they become almost imposible\n"
		"to other people to watch them\n"
		"To use Tochcrypt please try this options below:\n"
		"\n\n\n\t-e [VIDEOFILE]\t\t\tEncrypt input video\n"
		"\n\t-d [.TOCH FILE]\t\t\tDecrypt input file\n"
		"\n\t-gk\t\t\t\tGenerate a random compatible key\n"
		"\n\t-ik\t\t\t\tWill generate the inverse key from an input key\n"
		"\n\t-c ['']['']['']['']\t\tUse custom codec for decryption, please use the fourcc 4 character identifier\n\n"
		"THANK YOU for using TOCHCRYPT!!!\n\n"
		"\t\t                 __  \n"
		"\t\t                /   \  \n"
		"\t\t                | /| \  \n"
		"\t\t                | ||_/  \n"
		"\t\t                | ||  \n"
		"\t\t           /\    | ||  \n"
		"\t\t        /   |   | ||  \n"
		"\t\t       /  |\ |   | ||  \n"
		"\t\t     |__/|| |   | ||  \n"
		"\t\t         || |   | ||  \n"
		"\t\t         || |   | ||  \n"
		"\t\t        /    ~~~    \  \n"
		"\t\t       |   ^     ^   |  \n"
		"\t\t       |  (o)   (o)  |  \n"
		"\t\t       \      !      /  \n"
		"\t\t       >\     ^     /<  \n"
		"\t\t         \_________/  \n"
		"\t\t           (\   /)  \n"
		"\t\t           ( >o< )  \n"
		"\t\t           (/   \)  \n"
		);

	if(error == 1)
		printf( "\nTo use Tochcrypt please try this options below:\n"
			"\n\n\n\t-e [VIDEOFILE]\t\t\tEncrypt input video\n"
			"\n\t-d [.TOCH FILE]\t\t\tDecrypt input file\n"
			"\n\t-gk\t\t\t\tGenerate a random compatible key\n"
			"\n\t-ik\t\t\t\tWill generate the inverse key from an input key\n"
			"\n\t-c ['']['']['']['']\t\tUse custom codec for decryption, please use the fourcc 4 character identifier\n\n"
			);

	if(error == 2)
	{
		printf("Wrong Syntax! :(");
		tochHelp(1);
	}
	if(error == 3)
		printf("Invalid input key.\n");

	if(error == 4)
		printf("Wrong fourcc codec!\n");

	if(error == 5)
		printf("Input video could not be opened.\n");

	if(error == 6)
		printf("Output file could not be created, maybe not enought space left.\n");

	if(error == 7)
		printf("Input toch file could not be opened.\nMaybe is corrupted. :(\n");
}



//cudaPrintfDisplay(stdout, true);
//cudaPrintfEnd();
