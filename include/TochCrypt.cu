#include "TOCH_CUDA_KERNEL.h"
#include "TOCH_CUDA_KERNEL.cu"

int slash_out(const char *str)
{
    return (str && *str && str[strlen(str) - 1] == '/') ? 1 : 0;
}

Mat Tochcrypt(Mat picture, int *key, int keyNum, float *timeCuda)
{


	//printf("Me enviaron a ejecutar Tochcrypt");
	//Create pointers of uchar(0..255,X4) for original picture and device original picture
	uchar4 *originalPicture, *deviceOriginalPicture;

	//Create pointers but now of the output images
	uchar4 *outPicture, *deviceOutputPicture;

	//Create pointer for the GPU of the key the algorithm will apply
	int *dKey;

	//Free memory
	cudaFree(0);

	//Create another object but now for the image channel convertion
	Mat pictureRGBA;
	Mat resultPicture;


	//printf("%s\n", pictureName);
	// c_str() apuntador al nombre de la picture
	//picture = imread(pictureName.c_str(), CV_LOAD_IMAGE_COLOR);
	if( picture.empty() )
	{

		return resultPicture;
		printf("Error!\n");
	}
	else
	{
		//imshow("Video Original", picture);


		//Convert the colors from Blue Green Red to ==> Red Green Bluw
		cvtColor(picture, pictureRGBA, CV_BGR2RGBA);

		//Get the number of rows from picture object
		int rows = picture.rows;

		//Same for columns
		int columns = picture.cols;

		//Get the total number of pixels that will change
		const size_t totalPixelNumber = rows * columns;

		//Set the object to be the very first pixel of the converted RGB image
		originalPicture = (uchar4 *)pictureRGBA.ptr<unsigned char>(0);

		//Allocate an array for the total of pixels needed
		outPicture = (uchar4 *)malloc(sizeof(uchar4)*totalPixelNumber);

		//Divide the picture rows and columns by 4 and send each grid
		int N = 4, M = 4;

		//Calculate the grid size
		const dim3 blockSize(columns / N + 1, rows / M + 1, 1);

		//And how many threads we are going to use
		const dim3 threads(N, M, 1);

		//Reserve memory in the GPU for
		cudaMalloc(&dKey, (sizeof(int) * 9) * keyNum);  //The key for encryption
		cudaMalloc(&deviceOriginalPicture, sizeof(uchar4) * totalPixelNumber); //Original Picture
		cudaMalloc(&deviceOutputPicture, sizeof(uchar4) * totalPixelNumber);   //Output Picture

		//Set the output image to 0 to all pixels
		cudaMemset(dKey, 0, sizeof(int) * (9*keyNum));
		cudaMemset(deviceOutputPicture, 0, sizeof(uchar4) * totalPixelNumber);

		//Now we copy the information to the reserved memory
		cudaMemcpy(deviceOriginalPicture, originalPicture, sizeof(uchar4) * totalPixelNumber, cudaMemcpyHostToDevice); //Copy Original Picture to the device
		cudaMemcpy(dKey, key, (sizeof(int) * (9 * keyNum)), cudaMemcpyHostToDevice); //Copy key to the device

		//Start to get time for the gpu process
		clock_t GPUtimer = clock();
		//cudaPrintfInit();

		//START THE GPU KERNEL, ENCRYPTION TIME!
		PixelMutation <<<blockSize, threads>>> (deviceOriginalPicture, deviceOutputPicture, rows, columns, dKey, keyNum);

		//cudaPrintfDisplay(stdout, true);
		//cudaPrintfEnd();
		//Stop timer and print timing
		GPUtimer = clock() - GPUtimer;
		float timing=( (((float)GPUtimer) / CLOCKS_PER_SEC) * 1000 );
		*timeCuda=timing;

		//Wait until everything with the GPU is done
		cudaDeviceSynchronize();
		//printf("Acabe KERNEL->>>%f\n",timing);
		//Recover the picture from the GPU to the HOST
		cudaMemcpy(outPicture, deviceOutputPicture,sizeof(uchar4) * totalPixelNumber, cudaMemcpyDeviceToHost);

		//Create object for output picture
		Mat out(rows, columns, CV_8UC4, outPicture);

		//Convert it back to the BlueGreenRed Format so openCV can write the image correctly
		cvtColor(out, resultPicture, CV_RGBA2BGR);

		delete outPicture;
		out.release();
		pictureRGBA.release();
		cudaDeviceSynchronize();
		cudaFree(deviceOutputPicture);
		cudaFree(deviceOriginalPicture);
		cudaFree(dKey);
		return resultPicture;

	}
}




Mat TochDecrypt(Mat picture, int *key, int keyNum, float *timeCuda)
{


	//printf("Me enviaron a ejecutar Tochcrypt");
	//Create pointers of uchar(0..255,X4) for original picture and device original picture
	uchar4 *originalPicture, *deviceOriginalPicture;

	//Create pointers but now of the output images
	uchar4 *outPicture, *deviceOutputPicture;

	//Create pointer for the GPU of the key the algorithm will apply
	int *dKey;

	//Free memory
	cudaFree(0);

	//Create another object but now for the image channel convertion
	Mat pictureRGBA;
	Mat resultPicture;


	//printf("%s\n", pictureName);
	// c_str() apuntador al nombre de la picture
	//picture = imread(pictureName.c_str(), CV_LOAD_IMAGE_COLOR);
	if( picture.empty() )
	{

		return resultPicture;
		printf("Error!\n");
	}
	else
	{
		//imshow("Video Original", picture);


		//Convert the colors from Blue Green Red to ==> Red Green Bluw
		cvtColor(picture, pictureRGBA, CV_BGR2RGBA);

		//Get the number of rows from picture object
		int rows = picture.rows;

		//Same for columns
		int columns = picture.cols;

		//Get the total number of pixels that will change
		const size_t totalPixelNumber = rows * columns;

		//Set the object to be the very first pixel of the converted RGB image
		originalPicture = (uchar4 *)pictureRGBA.ptr<unsigned char>(0);

		//Allocate an array for the total of pixels needed
		outPicture = (uchar4 *)malloc(sizeof(uchar4)*totalPixelNumber);

		//Divide the picture rows and columns by 4 and send each grid
		int N = 4, M = 4;

		//Calculate the grid size
		const dim3 blockSize(columns / N + 1, rows / M + 1, 1);

		//And how many threads we are going to use
		const dim3 threads(N, M, 1);

		//Reserve memory in the GPU for
		cudaMalloc(&dKey, (sizeof(int) * 9) * keyNum);  //The key for encryption
		cudaMalloc(&deviceOriginalPicture, sizeof(uchar4) * totalPixelNumber); //Original Picture
		cudaMalloc(&deviceOutputPicture, sizeof(uchar4) * totalPixelNumber);   //Output Picture

		//Set the output image to 0 to all pixels
		cudaMemset(deviceOutputPicture, 0, sizeof(uchar4) * totalPixelNumber);

		//Now we copy the information to the reserved memory
		cudaMemcpy(deviceOriginalPicture, originalPicture, sizeof(uchar4) * totalPixelNumber, cudaMemcpyHostToDevice); //Copy Original Picture to the device
		cudaMemcpy(dKey, key, (sizeof(int) * 9) * keyNum, cudaMemcpyHostToDevice); //Copy key to the device

		//Start to get time for the gpu process
		clock_t GPUtimer = clock();
		//cudaPrintfInit();

		//START THE GPU KERNEL, ENCRYPTION TIME!
		PixelMutationDecrypt <<<blockSize, threads>>> (deviceOriginalPicture, deviceOutputPicture, rows, columns, dKey, keyNum);

		//Stop timer and print timing
		GPUtimer = clock() - GPUtimer;
		float timing=( (((float)GPUtimer) / CLOCKS_PER_SEC) * 1000 );
		*timeCuda=timing;

		//Wait until everything with the GPU is done
		cudaDeviceSynchronize();
		//cudaPrintfDisplay(stdout, true);
		//cudaPrintfEnd();

		//Recover the picture from the GPU to the HOST
		cudaMemcpy(outPicture, deviceOutputPicture,sizeof(uchar4) * totalPixelNumber, cudaMemcpyDeviceToHost);

		//Create object for output picture
		Mat out(rows, columns, CV_8UC4, outPicture);

		//Convert it back to the BlueGreenRed Format so openCV can write the image correctly
		cvtColor(out, resultPicture, CV_RGBA2BGR);

		delete outPicture;
		out.release();
		pictureRGBA.release();
		cudaDeviceSynchronize();
		cudaFree(deviceOutputPicture);
		cudaFree(deviceOriginalPicture);
		cudaFree(dKey);
		return resultPicture;

	}
}
