//Device Functions
//Euclidan module for the hill cipher algorithm, will be used from device
__device__ int mod (int n, int m);

__device__ int *getXYZ(int R, int G, int B, int *keys, int keyNum);
__device__ int *getInverseXYZ(int R, int G, int B, int *keys, int keyNum);


__global__ void PixelMutation(uchar4* const originalPicture, uchar4* const outPicture, int rows, int columns, int *keyMatrix, int keyNum);

////////////////////////////////////////////////////////////////////
//GPU KERNEL, this is the parallel image encryption application
//INPUT:
//	1.- < originalPicture >
//		The original picture we are analyzing

//	2.- < outPicture >
//		The result of the manipulation of the pixels

//	3.- < rows >
//		The number of rows the picture has

//	4.- < columns >
//		The number of columns the picture has

//	5.- < keyMatrix>
//		The base matrix which can be the lock or unlock key
////////////////////////////////////////////////////////////////////
