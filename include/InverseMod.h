//	######################################################
//	#####		MODULAR INVERSE MATRIX		######
//	#####		RETURNS MATRIX	WITH		######
//	#####		THE MATRIX THE PIXELS		######
//	#####		WILL BE DECRYPTED		######
//	######################################################

//DECLARE THE FUNCTIONS WE ARE GOING TO CREATE
void printArray(int *array, int n);

//CALCULATE THE DETERMINANT OF A 3X3 and 2X2 MATRIX
//IT WILL BE USED FOR THE DECRYPT PROCESS
int determinant3(int *matrix);
int determinant2(int *matrix);

//Get the cofactor matrix
void coFactor(int *matrix, int n);

//Transpose Inverse
void transpose(int *matrix, int *fac, int n);

//Get the absolute value of a number
int abs(int);

//Will get the euclidean module
int mymod(int n, int m);

//Greatest common divisor recursive
int gcdr ( int, int );

//Get CoFactors using the formula
int coFactorFormula(int *, int );

//This function will return true or false, true if its an integer number and its for the modular multiplicative inverse
bool isInt(double number);

//Function which will get the transpose matrix
void transpose_matrix(int *cofactor, int *transpose_matrix, int n);

//Get the multiplicative inverse
int modular_inverse_multiplicative(int determinant);

//Get the multiplicative inverse
void generate_matrix(int n, int, int *generated_matrix);

//Get the inverse matrix given a matrix
void InverseMatrix (int *originalKey, int channels, int *inverse_array);


#include "InverseMod.c"
