//Device Functions



//Euclidan module for the hill cipher algorithm, will be used from device
__device__ int euMod (int n, int m)
{
	if( n < 0 )
	{
		if ( ((n % m) + m) == m )
		{
			return 0;
		}
		else
		{
			return ((n % m) + m);
		}
	}
	else
	{
		return n % m;
	}
}

//__device__ int *getXYZ(int R, int G, int B, int *keys, int keyNum, int thrd)
__device__ int *getXYZ(int R, int G, int B, int *keys, int keyNum)
{
	int i;
	int resultXYZ[3];
	/*if(thrd == 1){
		cuPrintf("R->%d\tG->%d\tB->%d\t X Y Z ME ACABAN DE LLAMAR\n", R, G, B);
		cuPrintf("####################INICIO GETXYZ########################\n");
	}
*/
	int x = ( keys[0] * R ) + ( keys[1] * G ) + ( keys[2] * B );
	int y = ( keys[3] * R ) + ( keys[4] * G ) + ( keys[5] * B );
	int z = ( keys[6] * R ) + ( keys[7] * G ) + ( keys[8] * B );
	x=euMod(x,256);
	y=euMod(y,256);
	z=euMod(z,256);
/*	if(thrd == 1)
		cuPrintf("x->%d\ty->%d\tz->%d\tINICIO SIN MODULO\n", x, y, z);
*/
	for(i=1; i<keyNum; i++)
	{
		int renglon=i*9;
		int myX=( keys[ renglon + 0 ] * x ) + ( keys[ renglon + 1 ] * y ) + ( keys[ renglon + 2 ] * z );
		int myY=( keys[ renglon + 3 ] * x ) + ( keys[ renglon + 4 ] * y ) + ( keys[ renglon + 5 ] * z );
		int myZ=( keys[ renglon + 6 ] * x ) + ( keys[ renglon + 7 ] * y ) + ( keys[ renglon + 8 ] * z );

/*		if(thrd == 1){
			cuPrintf("COLOR\tINDEX\tValue\n"
				"X\t%d\t%d\t* %d\n"
				"X\t%d\t%d\t* %d\n"
				"X\t%d\t%d\t* %d\n"
				, renglon+0, keys[ renglon + 0 ], x
				, renglon+1, keys[ renglon + 1 ], y
				, myX, myY, myZ
				);
				cuPrintf("x->%d\ty->%d\tz->%d\tSIN MODULO\n", myX, myY, myZ);
		}
*/
		x=euMod(myX,256);
		y=euMod(myY,256);
		z=euMod(myZ,256);
/*		if(thrd == 1)
			cuPrintf("x->%d\ty->%d\tz->%d\tCON MODULO\n", x, y, z);
*/

	}


	//cudaMalloc(&resultXYZ, sizeof(int) * 3);
	resultXYZ[0]=x;
	resultXYZ[1]=y;
	resultXYZ[2]=z;

	return resultXYZ;
}


int *getInverseXYZSeq(int R, int G, int B, int *keys, int keyNum)
{
	int i;
	int resultXYZ[3];
	int index=((keyNum-1)*9);
	int x = ( keys[index+0] * R ) + ( keys[index+1] * G ) + ( keys[index+2] * B );
	int y = ( keys[index+3] * R ) + ( keys[index+4] * G ) + ( keys[index+5] * B );
	int z = ( keys[index+6] * R ) + ( keys[index+7] * G ) + ( keys[index+8] * B );
	x=euMod(x,256);
	y=euMod(y,256);
	z=euMod(z,256);
	for(i=keyNum-2; i>=0; i--)
	{
		int myIndex=i*9;
		int myX = ( keys[ myIndex + 0 ] * x ) + ( keys[ myIndex + 1 ] * y ) + ( keys[ myIndex + 2 ] * z );
		int myY = ( keys[ myIndex + 3 ] * x ) + ( keys[ myIndex + 4 ] * y ) + ( keys[ myIndex + 5 ] * z );
		int myZ = ( keys[ myIndex + 6 ] * x ) + ( keys[ myIndex + 7 ] * y ) + ( keys[ myIndex + 8 ] * z );
/*		if(thrd == 1){
			cuPrintf("COLOR\tINDEX\tValue DECRYPT\n"
				"X\t%d\t%d\t* %d\n"
				"X\t%d\t%d\t* %d\n"
				"X\t%d\t%d\t* %d\n"
				, myIndex+0, keys[ myIndex ], x
				, myIndex+1, keys[ myIndex + 1 ], y
				, myX, myY, myZ
				);
				cuPrintf("x->%d\ty->%d\tz->%d\tSIN MODULO DECRYPT\n", myX, myY, myZ);
		}
*/
		x=euMod(myX, 256);
		y=euMod(myY, 256);
		z=euMod(myZ, 256);

	}
//	if(thrd == 1)
//		cuPrintf("Retornando: R:%d\tG:%d\tB:%d\n", x, y, z);

	resultXYZ[0]=x;
	resultXYZ[1]=y;
	resultXYZ[2]=z;

	return resultXYZ;
}

////////////////////////////////////////////////////////////////////
//GPU KERNEL, this is the parallel image encryption application
//INPUT:
//	1.- < originalPicture >
//		The original picture we are analyzing

//	2.- < outPicture >
//		The result of the manipulation of the pixels

//	3.- < rows >
//		The number of rows the picture has

//	4.- < columns >
//		The number of columns the picture has

//	5.- < keyMatrix>
//		The base matrix which can be the lock or unlock key
////////////////////////////////////////////////////////////////////
void PixelMutationSeq(uchar4* const originalPicture, uchar4* const outPicture, int rows, int columns, int *keyMatrix, int keyNum)
{
	//Get the column, and row position depending on block and thread
	int column = (blockIdx.x * blockDim.x) + threadIdx.x;
	int row = (blockIdx.y * blockDim.y) + threadIdx.y;

	//If the position is out of the picture range
	if (column >= columns || row >= rows)
	{
		//Do nothing
		return;
	}
	for(i=0;i<rows;i++)
	{
		for(j=0;j<columns;j++)
		{
			//Get the position of the pixel which will change
			int index = i * columns + j;

			//Get the value of each channel of the pixel
			int red = originalPicture[index].x;
			int green = originalPicture[index].y;
			int blue = originalPicture[index].z;

			//Do the matrix multipĺication with keyMatrix and each pixel
			int *xyz=getXYZ(red, green, blue, keyMatrix, keyNum);
			int x=xyz[0];
			int y=xyz[1];
			int z=xyz[2];


			//Get the module using euclidean module for each channel
			//Euclidan module
			x=euMod(x,256);
			y=euMod(y,256);
			z=euMod(z,256);


			outPicture[index].x = (uchar)x;
			outPicture[index].y = (uchar)y;
			outPicture[index].z = (uchar)z;

			outPicture[index].w = originalPicture[index].w;  // alfa

		}
	}
}







__global__ void PixelMutationDecryptSeq(uchar4* const originalPicture, uchar4* const outPicture, int rows, int columns, int *keyMatrix, int keyNum)
{
	//Get the column, and row position depending on block and thread
	int column = (blockIdx.x * blockDim.x) + threadIdx.x;
	int row = (blockIdx.y * blockDim.y) + threadIdx.y;

	//If the position is out of the picture range
	if (column >= columns || row >= rows)
	{
		//Do nothing
		return;
	}

	//Get the position of the pixel which will change
	int index = row * columns + column;

	//Get the value of each channel of the pixel
	int red = originalPicture[index].x;
	int green = originalPicture[index].y;
	int blue = originalPicture[index].z;
	int thrd=0;
	int *xyz=getInverseXYZ(red, green, blue, keyMatrix, keyNum);
	int x=xyz[0];
	int y=xyz[1];
	int z=xyz[2];
	outPicture[index].x = (uchar)x;
	outPicture[index].y = (uchar)y;
	outPicture[index].z = (uchar)z;

	outPicture[index].w = originalPicture[index].w;  // alfa

}



