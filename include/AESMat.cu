//#include "TOCH_CUDA_KERNEL.h"
//#include "TOCH_CUDA_KERNEL.cu"
Mat AEScrypt(Mat picture, float *timeCuda)
{

	int sizePack = 16; // tamaño de paquetes en algoritmo
	//printf("Me enviaron a ejecutar Tochcrypt");
	//Create pointers of uchar(0..255,X4) for original picture and device original picture
	uchar4 *originalPicture, *deviceOriginalPicture;

	//Create pointers but now of the output images
	uchar4 *outPicture, *deviceOutputPicture;

	//Free memory
	cudaFree(0);

	//Create another object but now for the image channel convertion
	Mat pictureRGBA;
	Mat resultPicture;


	//printf("%s\n", pictureName);
	// c_str() apuntador al nombre de la picture
	//picture = imread(pictureName.c_str(), CV_LOAD_IMAGE_COLOR);
	if( picture.empty() )
	{

		return resultPicture;
		printf("Error!\n");
	}
	else
	{
		//imshow("Video Original", picture);


		//Convert the colors from Blue Green Red to ==> Red Green Bluw
		cvtColor(picture, pictureRGBA, CV_BGR2RGBA);

		//Get the number of rows from picture object
		int rows = picture.rows;

		//Same for columns
		int columns = picture.cols;

		//Get the total number of pixels that will change
		const size_t totalPixelNumber = rows * columns;

		int paquetes = ((((rows*columns) * 3) / sizePack) + 1);

		//Set the object to be the very first pixel of the converted RGB image
		originalPicture = (uchar4 *)pictureRGBA.ptr<unsigned char>(0);

		//Allocate an array for the total of pixels needed
		outPicture = (uchar4 *)malloc(sizeof(uchar4)*totalPixelNumber);

		//Divide the picture rows and columns by 4 and send each grid
		int N = 4, M = 4;

		//Calculate the grid size
		const dim3 blockSize(columns / N + 1, rows / M + 1, 1);

		//And how many threads we are going to use
		const dim3 threads(N, M, 1);

		//Reserve memory in the GPU for
		cudaMalloc(&deviceOriginalPicture, sizeof(uchar4) * totalPixelNumber); //Original Picture
		cudaMalloc(&deviceOutputPicture, sizeof(uchar4) * totalPixelNumber);   //Output Picture

		//Set the output image to 0 to all pixels
		cudaMemset(deviceOutputPicture, 0, sizeof(uchar4) * totalPixelNumber);

		//Now we copy the information to the reserved memory
		cudaMemcpy(deviceOriginalPicture, originalPicture, sizeof(uchar4) * totalPixelNumber, cudaMemcpyHostToDevice); //Copy Original Picture to the device

		//Start to get time for the gpu process
		clock_t GPUtimer = clock();
		//cudaPrintfInit();

		//START THE GPU KERNEL, ENCRYPTION TIME!
		uChar2Hex<<<blockSize, threads >> > (deviceOriginalPicture, deviceOutputPicture, rows, columns);


		int sizeBloques = sqrt(paquetes / 64);
		const dim3 gridSize2(sizeBloques + 1, sizeBloques + 1, 1);
		const dim3 blockSize2(64, 1);
		EncriptarHex2UChar<<< gridSize2, blockSize2 >> > (deviceOriginalPicture, deviceOutputPicture, rows, columns, sizePack);




		//cudaPrintfDisplay(stdout, true);
		//cudaPrintfEnd();
		//Stop timer and print timing
		GPUtimer = clock() - GPUtimer;
		float timing=( (((float)GPUtimer) / CLOCKS_PER_SEC) * 1000 );
		*timeCuda=timing;

		//Wait until everything with the GPU is done
		cudaDeviceSynchronize();
		//printf("Acabe KERNEL->>>%f\n",timing);
		//Recover the picture from the GPU to the HOST
		cudaMemcpy(outPicture, deviceOutputPicture,sizeof(uchar4) * totalPixelNumber, cudaMemcpyDeviceToHost);

		//Create object for output picture
		Mat out(rows, columns, CV_8UC4, outPicture);

		//Convert it back to the BlueGreenRed Format so openCV can write the image correctly
		cvtColor(out, resultPicture, CV_RGBA2BGR);

		delete outPicture;
		out.release();
		pictureRGBA.release();
		cudaDeviceSynchronize();
		cudaFree(deviceOutputPicture);
		cudaFree(deviceOriginalPicture);
		return resultPicture;

	}
}



