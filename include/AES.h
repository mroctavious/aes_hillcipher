
int contint;
int valorCajaS(int pos);
void expandirClave(unsigned char(*arrClave)[4], unsigned char *mClaveExp);
void addRoundKey(int pos, unsigned char(*texto)[4], unsigned char *mClaveExp);
void subBytes(unsigned char(*texto)[4]);
void shiftRows(unsigned char(*texto)[4]);
void mixColumns(unsigned char(*r)[4]);
void encriptar(unsigned char(*texto)[4], unsigned char *mClaveExp);
void mostrarIteraciones();
unsigned int* devIteraciones();
int hexadecimales(int numero);
void matriztemp(unsigned char *texto);
unsigned int iteraciones[176];
#include "AES.cpp"
