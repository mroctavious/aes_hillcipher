//Standard libraries the program need to work
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>
#include <string>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <errno.h>
#include <algorithm>

//CUDA Libraries
#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

//OpenCV Libraries
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>

//Namespaces
//Es para no ponder la api::funcion,
//Asi solo llamaremos la funcion() y listo
using namespace cv;
using namespace std;
#include "include/cudaAES.cu"
//#include "include/cAES.c"
#include "include/AESMat.cu"

//Create the macros
#define sorry printf("Sorry the matrix does not have solution\n"); exit(EXIT_FAILURE);

//Declare the constants
//The module is 256 because the value of the pixels go from 0 to 255
#define MODULE 256

#include "include/cuPrintf.cuh"
#include "include/cuPrintf.cu"

//My custom libraries for the program
#include "include/InverseMod.h"
#include "include/TochCrypt.cu"



int main(int argc, char *argv[])
{

	//Abrir imagen
	Mat picture=imread(argv[1], CV_LOAD_IMAGE_COLOR);

	int key[36]={ 216, 155, 3, 176, 248, 111, 71, 151, 205, 129, 212, 111, 174, 141, 16, 223, 164, 226, 15, 119, 157, 147, 234, 200, 158, 202, 127, 7, 213, 3, 142, 157, 218, 173, 204, 36 };
	int inverseKey[36]={ 85, 34, 79, 83, 129, 120, 88, 103, 144, 82, 36, 169, 4, 109, 218, 17, 232, 33, 150, 209, 70, 171, 115, 55, 130, 188, 25, 180, 208, 41, 126, 87, 252, 21, 79, 103 };
	printf("Key1:{ 216, 155, 3, 176, 248, 111, 71, 151, 205 };\n");
	printf("Inv1:{ 85, 34, 79, 83, 129, 120, 88, 103, 144 };\n");
	printf("Key2:{ 129, 212, 111, 174, 141, 16, 223, 164, 226 };\n");
	printf("Inv2:{ 82, 36, 169, 4, 109, 218, 17, 232, 33 }\n");
	float timeCuda=0.0;
	Mat encrypt=Tochcrypt(picture, key, 4, &timeCuda);
	imwrite(argv[2], encrypt);
	printf("Encriptacion tomo: %f\n", timeCuda );
	//Mat decrypt=Tochcrypt(picture, inverseKey);
	//imwrite(argv[3], decrypt);


	Mat cecy=AEScrypt(picture, &timeCuda);
	printf("Cecy tomo: %f\n", timeCuda );
	imwrite(argv[3], cecy);

	encrypt.release();
	picture.release();
	cecy.release();

	return 0;
}




