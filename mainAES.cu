#define _CRT_SECURE_NO_WARNINGS
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include "opencv2/imgproc/imgproc.hpp"


#include <iostream>
#include <stdio.h>
#include <math.h>
#include <string>
#include <time.h>
#include "include/AES.h"
#include <iomanip>
//#include <xiosbase>
#include "include/Hexa.h"
#include<iostream>
//AES aes;
//Hexa hexa;


using namespace cv;
using namespace std;
#include "include/cudaAES.cu"
#include "include/cAES.c"

int main(int argc, char **argv)
{
	uchar4 *imagenOriginal, *dImagenOriginal;
	uchar4 *imagenSalida, *dImagenSalida;
	uchar4 *imagenSalidaCPU;

	const string nombreImagen = argv[1];

	cudaFree(0);

	Mat imagen;
	Mat imagenRGBA;
	Mat salida1, salida2;

	float difTotal = 0;
	int nDiferentes = 0;

	// c_str() apuntador al nombre de la imagen
	imagen = imread(nombreImagen.c_str(), CV_LOAD_IMAGE_COLOR);

	if (imagen.empty()) {
		cout << "Imagen vacia...";
	}
	else {

		cvtColor(imagen, imagenRGBA, CV_BGR2RGBA);

		unsigned char equivHex[255];
		for (int i = 0; i < 255; i++){
			equivHex[i] = hexadecimal(i);
		}

		int filas = imagen.rows;
		int columnas = imagen.cols;
		int sizePack = 16; // tama�o de paquetes en algoritmo

		int paquetes = ((((filas*columnas) * 3) / sizePack) + 1);

		//printf( "Filas %d Columnas %d \n", filas, columnas);

		const size_t numPixeles = filas * columnas;
		imagenOriginal = (uchar4 *)imagenRGBA.ptr<unsigned char>(0);
		imagenSalida = (uchar4 *)malloc(sizeof(uchar4)*numPixeles);
		imagenSalidaCPU = (uchar4 *)malloc(sizeof(uchar4)*numPixeles);

		// definicion de clave en vector y en matriz	
		//unsigned char Llave[16] = { 0x6c, 0x61, 0x20, 0x76, 0x69, 0x64, 0x61, 0x20, 0x64, 0x73, 0x20, 0x62, 0x65, 0x6c, 0x6c, 0x61 }; //clave  
		//unsigned char Llave4x4[4][4]; //matriz 4x4 clave		
		//for (int i = 0; i < 4; i++){
		//	for (int j = 0; j < 4; j++){

		//	}
		//}

		clock_t timer1 = clock();
		//EncriptarCPU(imagenOriginal, imagenSalidaCPU,
		//	filas, columnas, sizePack);
		timer1 = clock() - timer1;

		printf("\n \nOperacion en CPU toma %10.3f ms.\n", (((float)timer1) / CLOCKS_PER_SEC) * 1000);

		int N = 4, M = 4;

		const dim3 gridSize1(columnas / N + 1, filas / M + 1, 1);
		const dim3 blockSize1(N, M, 1);

		cudaMalloc(&dImagenOriginal, sizeof(uchar4) * numPixeles);
		cudaMalloc(&dImagenSalida, sizeof(uchar4) * numPixeles);
		cudaMemset(dImagenSalida, 0, sizeof(uchar4) * numPixeles);
		cudaMemcpy(dImagenOriginal, imagenOriginal,
			sizeof(uchar4) * numPixeles, cudaMemcpyHostToDevice);

		clock_t timer2 = clock();
		uChar2Hex << <gridSize1, blockSize1 >> >
			(dImagenOriginal, dImagenSalida,
			filas, columnas);

		int sizeBloques = sqrt(paquetes / 64);

		const dim3 gridSize2(sizeBloques + 1, sizeBloques + 1, 1);
		const dim3 blockSize2(64, 1);

		EncriptarHex2UChar	 <<<gridSize2, blockSize2 >> >
			(dImagenOriginal, dImagenSalida,
			filas, columnas, sizePack);

		timer2 = clock() - timer2;
		printf("Operacion en Device toma %10.3f ms.\n", (((float)timer2) / CLOCKS_PER_SEC) * 1000);

		cudaDeviceSynchronize();
		cudaMemcpy(imagenSalida, dImagenSalida,
			sizeof(uchar4) * numPixeles, cudaMemcpyDeviceToHost);

		difTotal = CompararImgs(imagenSalida, imagenSalidaCPU, filas, columnas, &nDiferentes);
		/*difTotal = difTotal + 300;
		nDiferentes = nDiferentes + 15;*/
		printf("Elementos diferentes %d (%.3f Porcentaje) Con valor de %f\n", nDiferentes, ((nDiferentes / float(numPixeles)) * 100), difTotal);

		Mat temp1(filas, columnas, CV_8UC4, imagenSalidaCPU);
		Mat temp2(filas, columnas, CV_8UC4, imagenSalida);

		cvtColor(temp1, salida1, CV_RGBA2BGR);
		cvtColor(temp2, salida2, CV_RGBA2BGR);
		PrintTxt(imagenSalida, filas, columnas);

		imshow("original", imagen);
		imshow("salida CPU", salida1);
		imshow("salida GPU", salida2);

		imwrite("salidaCPU.png", salida1);
		imwrite("salidaGPU.png", salida2);

		waitKey(0);
	};
}
