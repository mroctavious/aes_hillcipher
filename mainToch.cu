//###############################################################
//
//	           )          )       (       ) (
//	  *   ) ( /(   (   ( /(   (   )\ ) ( /( )\ )  *   )
//	` )  /( )\())  )\  )\())  )\ (()/( )\()|()/(` )  /(
//	 ( )(_)|(_)\ (((_)((_)\ (((_) /(_)|(_)\ /(_))( )(_))
//	(_(_())  ((_))\___ _((_))\___(_))__ ((_|_)) (_(_())
//	|_   _| / _ ((/ __| || ((/ __| _ \ \ / / _ \|_   _|
//	  | |  | (_) | (__| __ || (__|   /\ V /|  _/  | |
//	  |_|   \___/ \___|_||_| \___|_|_\ |_| |_|    |_|
//
//		@Authors: Sandra Luz Canchola
//			  Octavio Rodriguez Garcia
//
//		UNIVERSIDAD AUTONOMA DE QUERETARO 2017
//
//	Description: This program uses the Hill Cipher Algorithm
//	to cipher pixel by pixel for each color channel(RGB)
//	of a single picture. TOCHCRYPT is the program which will
//	combine the power of parallel programming with CUDA, FFMPEG,
//	other programs. All to create a secure encrypted video, so
//	it can be sent through internet.
//
//	The program steps are this:
//	1: Get the Video and transform it into pictures
//
//	2: Give the program an input folder with the video pictures
//
//	3: For each picture in the folder apply the algorithm with
//	   the key the user input and follow step 5, if its a decrypt
//	   process, procceed to the step 4
//
//	4: Get the Modular inverse matrix from the matrix the user
//	   gave and apply the encryption process with the inverse key
//
//	5: Save all images into a folder
//
//	6: Use FFMMPEG to convert all images into a video, in this step
//	   music or sound can be added to the video
//Ejemplo de ejecucion
//./Tochcrypt -e ~/Videos/myVideo.mov 17 17 5 21 18 21 2 2 19 -o outSeq%03d.png -c R G B T
//###############################################################


//Standard libraries the program need to work
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>
#include <string>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <errno.h>
#include <algorithm>
//CUDA Libraries
#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

//OpenCV Libraries
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>

//Namespaces
//Es para no ponder la api::funcion,
//Asi solo llamaremos la funcion() y listo
using namespace cv;
using namespace std;

//Create the macros
#define sorry printf("Sorry the matrix does not have solution\n"); exit(EXIT_FAILURE);

//Declare the constants
//The module is 256 because the value of the pixels go from 0 to 255
#define MODULE 256

//My custom libraries for the program
#include "include/cuPrintf.cuh"
#include "include/cuPrintf.cu"
#include "include/crypp.h"
#include "include/InverseMod.h"
#include "include/TochCrypt.cu"


typedef enum STATUS
{
	EMPTY=0,
	OK=1,
	FAIL=-1,
	UNKNOWN=99
}Status;

typedef enum HEADER
{
	FPS=0,
	RESX=1,
	RESY=2,
	TFRAMES=3
}Header;

//Prototypes
void tochHelp(int);
int tochCryptVideo(char *inputVideo, char *outputVideoFile, char fCodec[], int *KEY, FILE *KEYFILE, int NUMBER_OF_KEYS );
int tochDecryptVideo(char *inputFolder, char *inputKey, char *outputVideoFile, char fCodec[]);

Status ARGV_ENCRYPT=EMPTY;
Status ARGV_DECRYPT=EMPTY;
Status ARGV_OUTNAME=EMPTY;
Status ARGV_RANDOMKEY=EMPTY;
Status ARGV_INPUTKEY=EMPTY;
Status ARGV_INVERSEINPUTKEY=EMPTY;
Status ARGV_CODEC=EMPTY;
Status INPUT_STATUS=EMPTY;

//////////////////////////
//	MAIN PROGRAM	//
//////////////////////////
int main (int argc, char *argv[])
{
	//CREATE THE KEY WITH THE MAX SIZE POSIBLE FOR THIS PROGRAM
	int key[16];

	//Analyze the input, and check where to go
	int action=0;

	//Input video
	char *inputVideo;

	//Input key given from user
	char *inputKey;
	char *inputFolder;

	//Output video
	char *outputVideoFile;

	//How many channel we will modify, 3 or 4
	int channels=3;

	//Input codec, for debugging purpuses
	char fCodec[4];
	memset(fCodec, '\0', sizeof(fCodec) + 1);

	int i=0,j=0;



	//CHECK IF THE ARGUMENTS ARE IN CORRECT FORMAT
	while(i<argc-1)
	{
		//Will look for a correct input from user
		//Check if the user want to generate a key
		if( (strcmp(argv[i+1],"-gk") == 0) || (strcmp(argv[i+1],"-g") == 0) )
		{
			ARGV_RANDOMKEY=OK;
			++i;
		}

		//Check if the user want to know the inverse module of a matrix
		else if( (strcmp(argv[i+1],"-i") == 0) || (strcmp(argv[i+1],"-ik") == 0) )
		{
			++i;
			ARGV_INVERSEINPUTKEY=OK;
			key[0]=atoi(argv[i+1]);
			key[1]=atoi(argv[i+2]);
			key[2]=atoi(argv[i+3]);
			key[3]=atoi(argv[i+4]);
			key[4]=atoi(argv[i+5]);
			key[5]=atoi(argv[i+6]);
			key[6]=atoi(argv[i+7]);
			key[7]=atoi(argv[i+8]);
			key[8]=atoi(argv[i+9]);
			i+=9;

		}

		//Check if its a video codec
		else if( (strcmp(argv[i+1],"-c") == 0) )
		{
			ARGV_CODEC=OK;
			i++;
			fCodec[0]=argv[i+1][0];
			fCodec[1]=argv[i+2][0];
			fCodec[2]=argv[i+3][0];
			fCodec[3]=argv[i+4][0];
			i+=4;

		}

		//If the user wants to encrypt a video
		else if ( (strcmp(argv[i+1],"-d") == 0) ||  (strcmp(argv[i+1],"--decrypt") == 0))
		{
			ARGV_DECRYPT=OK;
			i++;
			//realloc(inputFolder, sizeof(char) * strlen(argv[i+1]) + 1 );
			//realloc(inputKey, sizeof(char) * strlen(argv[i+2]) + 1 );
			//strcpy(inputFolder, argv[i+1]);
			//strcpy(inputKey, argv[i+2]);
			inputFolder=argv[i+1];
			inputKey=argv[i+2];

		}
		//Check if the user has give a folder location
		else if( (strcmp(argv[i+1],"-e") == 0) )
		{
			ARGV_ENCRYPT=OK;
			++i;
			//realloc(inputVideo, sizeof(char) * strlen(argv[i+1]) + 1 );
			//memset(inputVideo, '\0', sizeof(char) * strlen(argv[i+1]) + 1 );
			//strcpy(inputVideo, argv[i+1]);
			inputVideo=argv[i+1];
			printf("Input Video: %s\n",inputVideo);

		}
		//Take the output dir name
		else if( (strcmp(argv[i+1],"-o") == 0) || (strcmp(argv[i+1],"-out") == 0) || (strcmp(argv[i+1],"-output") == 0 ) )
		{
			ARGV_OUTNAME=OK;
			++i;
			outputVideoFile=argv[i+1];
			printf("Out Folder: %s\n",outputVideoFile);

		}

		i++;

	}

	//If not enough args then it means that no correct input found in args
	if( ! ( ( (ARGV_ENCRYPT == OK || ARGV_DECRYPT == OK ) && (ARGV_OUTNAME == OK) ) || ARGV_INVERSEINPUTKEY == OK || ARGV_RANDOMKEY == OK) )
	{
		tochHelp(0);
		exit(EXIT_FAILURE);
	}







//Analize action and check if the user want to generate, encrypt or get the inverse
// 0=Error
// 1=Encrypt/Decrypt
// -1=Get inverse of a matrix
// 9=Generate a random and compatible matrix



///####Check if the user want to GENERATE  a random Key:---->
	else if( ARGV_RANDOMKEY == OK  )
	{

		printf("RANDOM MODULAR INVERSE MATRIX GENERATOR\nWORKING....\n");

		//Create temporal matrix which will have the generated compatible matrix
		int tmp_matrix[channels*channels];

		//Generate matrix
		generate_matrix(channels, (int)rand(), tmp_matrix);

		//Then just print it out
		for(j=0;j<channels;j++)
		{
			for(i=0;i<channels;i++)
			{
				printf("%d ",tmp_matrix[j*channels+i]);
			}
			printf("\n");
		}
	}

/////////////////////////////////////////////////////////////////////////









///####Check if the user want to get the key for the video restore:---->

	else if( ARGV_INVERSEINPUTKEY == OK )
	{
		///GET THE MODULAR INVERSE MATRIX
		printf("CALCULATING THE MODULAR INVERSE MATRIX...\n");

		//Create pointer which will point to the inverse array from function
		int *inverseMatrix;

		//Dynamically reserve the memory
		inverseMatrix=(int *)malloc( sizeof(int) * (channels*channels));

		//Call procedure to get the inverse matrix
		InverseMatrix(key, channels, inverseMatrix);

		//Print Result
		for(j=0;j<channels;j++)
		{
			for(i=0;i<channels;i++)
			{
				printf("%d ",inverseMatrix[(j*channels)+i]);
			}
			printf("\n");
		}
	/*	//Print Result
		for(j=0;j<channels;j++)
		{
			for(i=0;i<channels;i++)
			{
				printf("%d ",key[(j*channels)+i]);
			}
			printf("\n");
		}*/
		//Clean the memory
		free(inverseMatrix);
	}

////////////////////////////////////////////////////////////////////////////

	else if( ARGV_DECRYPT==OK )
	{
		if( ! fCodec[0] && ! fCodec[1] && ! fCodec[2] )
		{
			fCodec[0]='M';
			fCodec[1]='J';
			fCodec[2]='P';
			fCodec[3]='G';
		}

		//Name of the key file
		char *outFileName=(char *) malloc( sizeof(char) * (strlen(outputVideoFile)+5) );
		memset(outFileName, '\0', sizeof(outFileName));
		strcpy(outFileName, outputVideoFile);

		//Add the extension .toch
		strcat(outFileName, ".avi");
		printf("Decrypt\n");
		tochDecryptVideo(inputFolder, inputKey, outFileName, fCodec);
		free(outFileName);


	}

///####Check if the user want to encrypt a video:---->

	else if( ARGV_ENCRYPT==OK )
	{

		//Create pointer which will point to the inverse array from function
		//Dynamically reserve the memory
		//int *inverseKey=(int *)malloc( sizeof(int) * (channels*channels));

		//Call procedure to get the inverse matrix
		//InverseMatrix(key, channels, inverseKey);
		if( ! fCodec[0] && ! fCodec[1] && ! fCodec[2] )
		{
			//fCodec[0]='B';
			//fCodec[1]='G';
			//fCodec[2]='R';
			//fCodec[3]='T';

			fCodec[0]='M';
			fCodec[1]='J';
			fCodec[2]='P';
			fCodec[3]='G';
		}

		int encryptions=getEncNum();
		int keys[encryptions * 9];

		//Name of the key file
		char *keyFileName=(char *) malloc( sizeof(char) * (strlen(outputVideoFile)+6) );
		memset(keyFileName, '\0', sizeof(keyFileName));
		strcpy(keyFileName, outputVideoFile);

		//Add the extension .toch
		strcat(keyFileName, ".toch");

		//Create new file and start writting the keys
		FILE *archivo=fopen(keyFileName, "w");

		printf("Archivo llave de salida=%s\n", keyFileName);

		//Create temporal matrix which will have the generated compatible matrix
		int i,j;
		for(i=0; i<encryptions; i++)
		{
			int tmp_matrix[9];
			//Generate matrix
			generate_matrix(3, (int)rand(), tmp_matrix);
			copyKey(keys, tmp_matrix, i);
			printM(tmp_matrix, "Random generated");
		}

		int invKeys[9*encryptions];
		for(i=0;i<encryptions;i++)
		{
			//int invKey[9];
			int tmpKey[9];
			keyToCopy(keys, i, tmpKey);
			//InverseMatrix(tmpKey, 3, invKey);
			for(j=0;j<9;j++)
			{
				printf("%d  ",keys[ (i*9) + j ]);
			}
			printf(		"\n\n\n");
			/*for(j=0		;j<9;j++)
			{
				printf("Inv:%d  ",invKey[j]);
			}*/
			printEnc(tmpKey,archivo);
		}
		tochCryptVideo(inputVideo, outputVideoFile, fCodec, keys, archivo, encryptions);
		fclose(archivo);
		free(keyFileName);

	}

/*	else
	{
		inverseKey=(int *)malloc( sizeof(int) * (channels*channels));

		//Copy the key to an array, just to keep a back up and work with the program variables names
		memcpy( inverseKey, key, sizeof(int) * (channels*channels) );
	}

	for(j=0;j<3;j++)
	{
		for(i=0;i<channels;i++)
		{
			printf("%d ",inverseKey[j*channels+i]);
		}
		printf("\n");
	//}
	//	return tochCryptVideo(inputVideo, outputVideoFile, fCodec, inverseKey);

	//}*/
	return 0;
}







//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//		  FUNCTIONS HERE :D
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
int tochCryptVideo(char *inputVideo, char *output, char fCodec[], int *inverseKey, FILE* archivo, int keyNum)
{

	////////////////////////////////////////////
	//					////
	//	Start to open video		////
	////////////////////////////////////////////
	char *outputVid=(char *) malloc( sizeof(char) * (strlen(output) + 7) );
	memset(outputVid, '\0', sizeof(outputVid) );
	strcpy(outputVid, output);
	strcat(outputVid, "C.avi");
	char *outputVideoFile=(char *) malloc( sizeof(char) * (strlen(output) + 40) );
	memset(outputVideoFile, '\0', sizeof(outputVideoFile) );
	strcpy(outputVideoFile, output);
	strcat(outputVideoFile, "_0000000.png");
	printf("PKO Salida es %s\n",outputVideoFile);
//Intentar abrir archivo:
	//Abrir video y guardar en vCap
	VideoCapture vCap(inputVideo);

	//How many channel we will modify, 3 or 4
	int channels=3;
	int i,j;

	//Verificar si NO se pudo abrir el archivo
	if( ! vCap.isOpened() )
	{
		tochHelp(5);
		return 5;
	}

//Conseguir datos del video de entrada:

	//Conseguimos los frames por segundo FPS
	double fps= vCap.get(CV_CAP_PROP_FPS);
	//Resolucion eje X
	int ancho=vCap.get(CV_CAP_PROP_FRAME_WIDTH);
	//Resolucion eje Y
	int largo=vCap.get(CV_CAP_PROP_FRAME_HEIGHT);
	//Numero total de frames en el video
	float totalFrames=(float) vCap.get(CV_CAP_PROP_FRAME_COUNT);

	printf("FPS:%f\nAncho:%d   Largo:%d\nTotalFrames:%f\n",fps,ancho,largo,totalFrames);
	fprintf(archivo, "%f %d %d %f\n", fps, ancho, largo, totalFrames);
	float frameNum=0.0;

	//Crear objetos que guardaran y reproduciran el video:

	//Crear estructura donde guardaremos el video
	VideoWriter outputVideo;
	VideoWriter outputVideoTmp;

	//Crear constructor
	//int fourccNum=CV_FOURCC('R','G','B','T');
	int fourccNumPng=CV_FOURCC('R','G','B','T');
	int fourccNumVid=CV_FOURCC('M', 'J', 'P', 'G');
	outputVideo.open(outputVideoFile, fourccNumPng, fps, Size(ancho, largo));
	outputVideoTmp.open(outputVid, fourccNumVid, fps, Size(ancho, largo));
	//Verifica que se haya abierto el archivo
	if( ! outputVideo.isOpened() || ! outputVideoTmp.isOpened())
	{

		tochHelp(6);
		return 6;
	}

	int contadorImagenes=0;
	float totalTime=0;

	//Recorremos frame por frame:
	//Ciclo infinito para reproducir el video
	while(0==0)
	{
		//Creamos structura tipo Mat
		Mat foto;

		//Llamamos la funcion read() y lo guardaremos en la estructura foto
		bool hayFotograma = vCap.read(foto);

		//Verificamos si hay fotograma, de lo contrario significa que acabo
		if (! hayFotograma)
		{
			printf("\nNo hay mas fotogramas\n");
			break;

		}
		float *timing=(float *) malloc(sizeof(float) * 1);
		Mat encrypt=Tochcrypt(foto, inverseKey, keyNum, timing);
		totalTime=totalTime+*timing;

		//Mat Decrypt=Tochcrypt(encrypt,inverseMatrix);
		//Mostrar fotograma en la 'ventana del video'
		//imshow("Video Original", foto);
		//imshow("TochCrypt :)", encrypt);
		//imshow("TochCrypt Decrypt :)", Decrypt);

		//Escribir el fotograma en el archivo
		outputVideo.write(encrypt);
		outputVideoTmp.write(encrypt);

		//Liberamos las estructuras de opencv
		foto.release();
		encrypt.release();
		//Decrypt.release();
		free(timing);
		//Agregamos 1 para obtener el porcentaje
		frameNum++;
		printf("\rCurrent Progress: %.2f %%", ((frameNum * 100.0) / totalFrames));

		//Es necesario esta parte, de lo contrario no muestra nada
		if(cvWaitKey(33) == 27)
		{
			break;
		}

	}
	printf("Suma de tiempos:%.4f\nTotal Frames:%f\nPromedio:%.4f\n", totalTime, totalFrames, totalTime/totalFrames);
	outputVideo.release();
	outputVideoTmp.release();
	//free(inverseMatrix);
	free(outputVideoFile);
	free(outputVid);
	destroyAllWindows();

	return 0;
}




int tochDecryptVideo(char *inputFolder, char *inputKey, char *outputVideoFile, char fCodec[])
{

	////////////////////////////////////////////
	//					////
	//	Start to open video		////
	////////////////////////////////////////////

	//Intentar abrir archivo:

	//How many channel we will modify, 3 or 4
	int channels=3;
	int i,j;
/*
	Pasos:
		1.-Deencryptar llave
		2.-Verificar que llave no este dañada
		3.-Empezar a deEncryptar
	//Conseguir datos del video de entrada:
	VideoCapture vCap(outputVideoFile);
*/

	//Proceso de adquisicion de llave
	int lines=tochLines(inputKey) - 1;

	//Reservar memoria para la llave
	int *fKey=(int *) malloc(sizeof(int) * ( 9 * lines ) );

	//Conseguir llave e informacion del header del video
	float *videoInfoReturn=tochDecrypt(inputKey, fKey, lines);

	//printf("Video info:\n\tFPS:%f\n\tRESX:%d\n\tRESY:%d\n\tTFRAMES:%f\n",videoInfoReturn[0], (int) videoInfoReturn[1], (int) videoInfoReturn[2], videoInfoReturn[3]);


	//Conseguimos los frames por segundo FPS
	double fps= videoInfoReturn[0];
	//Resolucion eje X
	int ancho=(int) videoInfoReturn[1];
	//Resolucion eje Y
	int largo=(int) videoInfoReturn[2];
	//Numero total de frames en el video
	float totalFrames=videoInfoReturn[3];

	//printf("FPS:%f\nAncho:%d   Largo:%d\nTotalFrames:%f\n",fps,ancho,largo,totalFrames);
	float frameNum=0.0;

//Crear objetos que guardaran y reproduciran el video:

	//Crear estructura donde guardaremos el video
	VideoWriter outputVideo;
	int fourccNum;
	//Crear constructor
	fourccNum=CV_FOURCC(fCodec[0],fCodec[1],fCodec[2],fCodec[3]);

	//outputVideo.open(outputVideoFile, CV_FOURCC('R','G','B','A'), fps, Size(ancho, largo));
	outputVideo.open(outputVideoFile, fourccNum, fps, Size(ancho, largo));
	//outputVideo.open(outputVideoFile, -1, fps, Size(ancho, largo));
	//Verifica que se haya abierto el archivo
	if( ! outputVideo.isOpened() )
	{
		tochHelp(6);
		return 6;
	}




	//Creamos una ventana en el OS y dentro pondremos los videos
	///GET THE MODULAR INVERSE MATRIX

	//Create pointer which will point to the inverse array from function
	int *inverseMatrix;

	//Dynamically reserve the memory
	inverseMatrix=(int *)malloc( sizeof(int) * (9*lines));


/*	for(i=lines-1; i>-1; i--)
	{
		int invKey[9];
		int tmpKey[9];
		keyToCopy(fKey, i, tmpKey);
		InverseMatrix(tmpKey, 3, invKey);
		printf("\n\n\n");
		for(j=0;j<9;j++)
		{
			//printf("Inv:%d  lines-i=%d    lines:%d\n",invKey[j], (lines-1)-i, lines);
		}
		copyKey(inverseMatrix, invKey, (lines-1)-i);
	}
*/
	for(i=0; i<lines; i++)
	{
		int invKey[9];
		int tmpKey[9];
		keyToCopy(fKey, i, tmpKey);
		InverseMatrix(tmpKey, 3, invKey);
		printf("\n\n\n");
		for(j=0;j<9;j++)
		{
			printf("Inv:%d  lines-i=%d    lines:%d\n",invKey[j], i, lines);
		}
		copyKey(inverseMatrix, invKey, i);
	}


		//The objects we are going to need
		DIR *directory;

		//Data structure for each file in directory
		struct dirent *dir;

		//Try open directory
		directory = opendir(inputFolder);

		int files=0;
		//If its open
		if (directory)
		{
			// Scanning the in directory
			while ((dir = readdir(directory)) != NULL)
			{
				if (!strcmp (dir->d_name, "."))
					continue;
				if (!strcmp (dir->d_name, ".."))
					continue;
				if ( dir->d_name[0] == '.' )
					continue;
				char *myDir=dir->d_name;
				//printf("Encontro Archivo:\t%s\n",myDir);
				files++;
			}

			directory = opendir(inputFolder);

			char *sortedFiles[files];

			memset(sortedFiles, '\0', sizeof(sortedFiles));
			i=0;
			// Scanning the in directory
			while ((dir = readdir(directory)) != NULL)
			{
				//Ignore current folder '.'
				if (!strcmp (dir->d_name, "."))
					continue;

				//Ignore the back folder '..'
				if (!strcmp (dir->d_name, ".."))
					continue;

				//If its a hidden file, then ignore
				if ( dir->d_name[0] == '.' )
					continue;

				//Get file name
				char *myDir=dir->d_name;

				//Allocate the necesary memory to save the name of the files found
				sortedFiles[i]=(char*)malloc(sizeof(char)*strlen(myDir) + 2);

				//Copy the name into the array
				strcpy(sortedFiles[i], myDir);

				i++;
			}

			//Mandamos a ordenar los archivos
			strSort(files, sortedFiles);
			//Video de salida
			VideoWriter outputVideo;

			//Intentar crear archivo de salida
			outputVideo.open(outputVideoFile, fourccNum, fps, Size(ancho, largo));

			//Verifica que se haya abierto el archivo
			if( ! outputVideo.isOpened() )
			{
				tochHelp(6);
				return 6;
			}
			float totalTime=0;
			for(i=0; i<files; i++)
			{
				int inputFolderstrLen=strlen(inputFolder);
				int fileLen=strlen(sortedFiles[i]);
				char *inputPic=(char *) malloc( sizeof(char) *  (inputFolderstrLen + fileLen) + 20 );
				strcpy(inputPic,inputFolder);
				strcat(inputPic, "/");
				strcat(inputPic,sortedFiles[i]);

				//int tmpKey[9];
				//keyToCopy(inverseMatrix, , tmpKey);
				Mat picture= imread(inputPic, CV_LOAD_IMAGE_COLOR);
				float *timing=(float *) malloc(sizeof(float) * 1);

				Mat decrypt=TochDecrypt(picture, inverseMatrix, lines, timing);
				totalTime+=*timing;
				//imshow("TochCrypt Decrypt :)", decrypt);

				//Escribir el fotograma en el archivo
				outputVideo.write(decrypt);
				free(timing);
				free(inputPic);
				picture.release();
				decrypt.release();
				//Es necesario esta parte, de lo contrario no muestra nada
				if(cvWaitKey(33) == 27)
				{
					break;
				}
			}
			printf("Suma de tiempos:%.4f\nTotal Frames:%f\nPromedio:%.4f\n", totalTime, totalFrames, totalTime/totalFrames);

			outputVideo.release();

			/*

			//Ya que tenemos las llaves ordenadas y los archivos tambien, es hora de deencryptar
			for(i=0; i<lines; i++)
			{
				int tmpKey[9];
				keyToCopy(inverseMatrix, i, tmpKey);
			}
			*/
			//Liberar arreglo que contenia los nombres de los archivos
			for(i=0;i<lines;i++)
			{
				free(sortedFiles[i]);
			} /*
		*/
		}
			/*	char *tmpPictureName=dir->d_name;
				char *nextPicture=(char *) malloc(1 + strlen(input_folder)+ strlen(tmpPictureName) );
				strcpy(nextPicture, input_folder);
				strcat(nextPicture, tmpPictureName);

				Tochcrypt(nextPicture, inverseKey, output_folder, tmpPictureName);
				//printf("%s\n", nextPicture);
			}
			closedir(directory);
		}
/*
	//Creamos una ventana en el OS y dentro pondremos los videos
	//namedWindow("Video Original", CV_WINDOW_AUTOSIZE);
	//namedWindow("TochCrypt :)", CV_WINDOW_AUTOSIZE);

	///GET THE MODULAR INVERSE MATRIX

	//Create pointer which will point to the inverse array from function
	int *inverseMatrix;

	//Dynamically reserve the memory
	inverseMatrix=(int *)malloc( sizeof(int) * (3*3));

	//Call procedure to get the inverse matrix
	InverseMatrix(inverseKey, channels, inverseMatrix);

	//Print Result
	for(j=0;j<channels;j++)
	{
		for(i=0;i<channels;i++)
		{
			printf("%d ",inverseMatrix[j*channels+i]);
		}
		printf("\n");
	}

	//Recorremos frame por frame:

*/
	free(inverseMatrix);
	free(videoInfoReturn);
	free(fKey);
	destroyAllWindows();
	return 0;

}

void tochHelp(int error)
{
	if(error == 0)
		printf( "Tochcrypt will encrypt your videos so they become almost imposible\n"
		"to other people to watch them\n"
		"To use Tochcrypt please try this options below:\n"
		"\n\n\n\t-e [VIDEOFILE]\t\t\tEncrypt input video\n"
		"\n\t-d [.TOCH FILE]\t\t\tDecrypt input file\n"
		"\n\t-gk\t\t\t\tGenerate a random compatible key\n"
		"\n\t-ik\t\t\t\tWill generate the inverse key from an input key\n"
		"\n\t-c ['']['']['']['']\t\tUse custom codec for decryption, please use the fourcc 4 character identifier\n\n"
		"THANK YOU for using TOCHCRYPT!!!\n\n"
		"\t\t                 __  \n"
		"\t\t                /   \  \n"
		"\t\t                | /| \  \n"
		"\t\t                | ||_/  \n"
		"\t\t                | ||  \n"
		"\t\t           /\    | ||  \n"
		"\t\t        /   |   | ||  \n"
		"\t\t       /  |\ |   | ||  \n"
		"\t\t     |__/|| |   | ||  \n"
		"\t\t         || |   | ||  \n"
		"\t\t         || |   | ||  \n"
		"\t\t        /    ~~~    \  \n"
		"\t\t       |   ^     ^   |  \n"
		"\t\t       |  (o)   (o)  |  \n"
		"\t\t       \      !      /  \n"
		"\t\t       >\     ^     /<  \n"
		"\t\t         \_________/  \n"
		"\t\t           (\   /)  \n"
		"\t\t           ( >o< )  \n"
		"\t\t           (/   \)  \n"
		);

	if(error == 1)
		printf( "\nTo use Tochcrypt please try this options below:\n"
			"\n\n\n\t-e [VIDEOFILE]\t\t\tEncrypt input video\n"
			"\n\t-d [.TOCH FILE]\t\t\tDecrypt input file\n"
			"\n\t-gk\t\t\t\tGenerate a random compatible key\n"
			"\n\t-ik\t\t\t\tWill generate the inverse key from an input key\n"
			"\n\t-c ['']['']['']['']\t\tUse custom codec for decryption, please use the fourcc 4 character identifier\n\n"
			);

	if(error == 2)
	{
		printf("Wrong Syntax! :(");
		tochHelp(1);
	}
	if(error == 3)
		printf("Invalid input key.\n");

	if(error == 4)
		printf("Wrong fourcc codec!\n");

	if(error == 5)
		printf("Input video could not be opened.\n");

	if(error == 6)
		printf("Output file could not be created, maybe not enought space left.\n");

	if(error == 7)
		printf("Input toch file could not be opened.\nMaybe is corrupted. :(\n");
}



//cudaPrintfDisplay(stdout, true);
//cudaPrintfEnd();
